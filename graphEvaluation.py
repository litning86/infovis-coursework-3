import sys
import platform
import os
import random
import time
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
from matplotlib.ticker import FuncFormatter

degree_classification = ["A) First", "B) 2.1", "C) 2.2", "D) Third", "E) Fail"]

def create_random_datapoints():
    start = 100
    error_count = 0
    tempNum = 4
    total = 0
    tempList = []
    data_list = []

    for i in range(4):
        #print(tempList)
        n = random.randint(3, (100-total-(3*tempNum)))
        while n in tempList:
            n = random.randint(3, (100-total-(3*tempNum)))
            error_count = error_count + 1
            if error_count > 5:
                tempList = []
                return tempList

        tempList.append(n)
        total = total + n
        tempNum = tempNum - 1

    if (start - total) in tempList:
        tempList = []
        return tempList
    tempList.append(start - total)

    return tempList


def bar_charts(data, i):                              #first, 2.1, 2.2, third
    '''
        Description:

        Parameters:

        outputs:
    '''
    is_correct = True

    y_axis = np.arange(len(degree_classification))
    fig, ax = plt.subplots()

    plt.bar(y_axis, data, align='center', alpha=0.5)
    plt.xticks(y_axis, degree_classification)
    plt.yticks(np.arange(0, 110, step=10))
    formatter = FuncFormatter(lambda y, pos: "%d%%" % (y))
    ax.yaxis.set_major_formatter(formatter)
    plt.ylabel('Degree Classification')
    plt.title('Percentage of achieved degree classifications')

    plt.show(block=False)
    correct_answer = create_questions(i, data)
    #print(str(correct_answer))

    start_time = time.time()
    userInput = input("Please Enter your answer: ")
    if convert_number_char(userInput) == correct_answer:
        plt.close()
        end_time = time.time()
        is_correct = True
    else:
        plt.close()
        end_time = time.time()
        is_correct = False

    return (end_time - start_time), is_correct


def pie_charts(data, i):
    is_correct = True
    y = np.array(data)

    plt.pie(y, labels=degree_classification, autopct='%1.1f%%')
    plt.title('Percentage of achieved degree classifications')
    plt.show(block=False)
    correct_answer = create_questions(i, data)
    #print(str(correct_answer))

    start_time = time.time()
    userInput = input("Please Enter your answer: ")
    if convert_number_char(userInput) == correct_answer:
        plt.close()
        end_time = time.time()
        is_correct = True
    else:
        plt.close()
        end_time = time.time()
        is_correct = False

    return (end_time - start_time), is_correct

def convert_number_char(user_answer):
    num_answer = 0
    if user_answer.upper() == 'A':
        num_answer = 0
    if user_answer.upper() == 'B':
        num_answer = 1
    if user_answer.upper() == 'C':
        num_answer = 2
    if user_answer.upper() == 'D':
        num_answer = 3
    if user_answer.upper() == 'E':
        num_answer = 4
    return num_answer


def create_questions(q_num, data):
    '''
        Description:

        Parameters:
        integer: q_num
        list of integers: data

        outputs:
        returns answer which is an integer and acts as the correct answer to the
        question.
    '''

    question_list = []
    tempval = 0
    answer = 0

    if q_num is 0:
        print("1) Which degree classification was the most common?\n")
        tempval = 0
        for item in data:
            if item > tempval:
                tempval = item
        answer = data.index(tempval)
    elif q_num is 1:
        print("2) Which degree classification has the closet to 30%?\n")
        tempval = 100
        for item in data:
            if abs(30 - item) < tempval:
                tempval = abs(30 - item)
                tempItem = item
        answer = data.index(tempItem)
    elif q_num is 2:
        print("3) Which degree classification was the second least common?\n")
        sorted_list = sorted(data)
        tempval = sorted_list[1]
        answer = data.index(tempval)
    elif q_num is 3:                                                                        #first, 2.1, 2.2, third, fail
        print("4) What percentage of people passed their degree?\n")
        print("A) = 0-19.9%\n")
        print("B) = 20-39.9%\n")
        print("C) = 40-59.9%\n")
        print("D) = 60-79.9%\n")
        print("E) = 80-100%\n")
        for num in range(4):
            tempval = tempval + data[num]
        answer = get_answer_value(tempval)
    elif q_num is 4:
        print("5) What percentage of people got a 2.1 or above?\n")
        print("A) = 0-19.9%\n")
        print("B) = 20-39.9%\n")
        print("C) = 40-59.9%\n")
        print("D) = 60-79.9%\n")
        print("E) = 80-100%\n")
        for num in range(2):
            tempval = tempval + data[num]
        answer = get_answer_value(tempval)
    elif q_num is 5:
        print("6) What percentage of people failed their degree?\n")
        print("A) = 0-19.9%\n")
        print("B) = 20-39.9%\n")
        print("C) = 40-59.9%\n")
        print("D) = 60-79.9%\n")
        print("E) = 80-100%\n")
        tempval = data[4]
        answer = get_answer_value(tempval)
    elif q_num is 6:
        print("7) Which grade had the lowest percentage?\n")
        tempval = min(data)
        answer = data.index(tempval)
    elif q_num is 7:
        print("8) What percentage of people got below a first but did NOT fail?\n")
        print("A) = 0-19.9%\n")
        print("B) = 20-39.9%\n")
        print("C) = 40-59.9%\n")
        print("D) = 60-79.9%\n")
        print("E) = 80-100%\n")
        tempval = data[1] + data[2] + data[3]
        answer = get_answer_value(tempval)
    elif q_num is 8:
        print("9) Which degree classification has the closet to 50%?\n")
        tempval = 100
        for item in data:
            if abs(50 - item) < tempval:
                tempval = abs(50 - item)
                tempItem = item
        answer = data.index(tempItem)
    elif q_num is 9:
        print("10) Which degree classification had the closest percentage to a first")
        tempval = 100
        for item in data[1:]:
            if abs((data[0]) - item) < tempval:
                tempval = abs((data[0]) - item)
                tempItem = item
        answer = data.index(tempItem)

    return answer


def get_answer_value(tempval):
        if tempval > 0 and tempval < 20:
            answer = 0
        elif tempval > 19 and tempval < 40:
            answer = 1
        elif tempval > 39 and tempval < 60:
            answer = 2
        elif tempval > 59 and tempval < 80:
            answer = 3
        elif tempval > 79:
            answer = 4

        return answer


def main():
    bar_times = []
    pie_times = []
    q_correct_bar = []
    q_correct_pie = []

    if platform.system() is "Linux":
        clear = lambda: os.system('clear')
    elif platform.system() is "Windows":
        clear = lambda: os.system('cls')
    clear()

    for i in range(10):
        data_list = []

        while data_list == []:
            data_list = create_random_datapoints()

        answer_time, correctness = bar_charts(data_list, i)
        if correctness == True:
            q_correct_bar.append("Correct")
        else:
            q_correct_bar.append("Incorrect")
        bar_times.append(answer_time)

        clear()
        time.sleep(1)

    input("Press any key to progress onto the pie chart trial")

    for i in range(10):
        data_list = []

        while data_list == []:
            data_list = create_random_datapoints()

        answer_time, correctness = pie_charts(data_list, i)
        if correctness == True:
            q_correct_pie.append("Correct")
        else:
            q_correct_pie.append("Incorrect")
        pie_times.append(answer_time)

        clear()
        time.sleep(1)


    f = open("chartime.txt", "w")
    f.write("Bar Chart Times:\n")
    for item in bar_times:
        f.write(str(item) + "\n")
    for item in q_correct_bar:
        f.write(str(item) + "\n")
    f.write("\nPie Chart Times:\n")
    for item in pie_times:
        f.write(str(item) + "\n")
    for item in q_correct_pie:
        f.write(str(item) + "\n")
    f.close()


    print("Bar Chart Times: " + str(bar_times))
    print("\n")
    print("Pie Chart Times: " + str(pie_times))


if __name__ == "__main__":
    main()
